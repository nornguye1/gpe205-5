﻿//Norman Nguyen
//Powerup Rapid Fire
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
//Inherit from the Powerup base class
public class PowerupRapidFire : Powerup
{
    //Bonus Amount
    public float bonusAmount;

    public PowerupRapidFire(PowerupRapidFire powerupToClone)
    {
        bonusAmount = powerupToClone.bonusAmount;
        countdown = powerupToClone.countdown;
        isPerm = powerupToClone.isPerm;

        powerupType = PowerupType.RapidFirePowerup;
    }

    //Override Method
    public override void OnAddPowerup(TankData data)
    {
        //Decrease the shooting rate
        data.shootRate -= bonusAmount;

        //base function
        base.OnAddPowerup(data);
    }
    //Override Method
    public override void OnRemovePowerup(TankData data)
    {

        //base function
        base.OnRemovePowerup(data);

        //Returns back to normal
        data.shootRate += bonusAmount;
    }
}

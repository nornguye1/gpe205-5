﻿//Norman Nguyen
//This is the main AI Controller for the 4 AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    //****Tank Components****
    [HideInInspector]public TankData data;
    public List<Transform> waypoints; //Transform Array
    public int currentWaypoint = 0; //currentWaypoints
    public float waypointCutoff; //Waypoint Cutoff
    public bool isAdvancingWaypoints = true; //Advance waypoints for the Tank Patrol Enum
    //Patrol Enums
    public enum TankPatrol
    {
        aiLoop, aiPingPong
    };
    public TankPatrol tankPatrol;
    //****AVOIDANCE****
    public enum AvoidState
    {
        Normal, TurnToAvoid, MoveToAvoid
    }
    public AvoidState avoidState = AvoidState.Normal; //Normal State
    public float avoidDuration = 2.0f; //Avoid Duration
    private float avoidStateTime; // Avoid State Time
    public float avoidDistance = 5.0f; //Avoid Distance
    //Start Data
    void Start()
    {
        data = GetComponent<TankData>();
    }
    //Patrol
    public void Patrol()
    {
        //Using the MoveTowards method where you're not going for the tank (Chase), but instead to the waypoints based on the waypoints list.
        MoveTowards(waypoints[currentWaypoint].position);

        // Switch to a different waypoint
        if (Vector3.Distance(data.motor.tf.position, waypoints[currentWaypoint].position) <= waypointCutoff)
        {
            //Determine the Next Waypoints Advanced
            DetermineNextWayPointAdvanced();
        }
    }
    //Advanced Waypoints
    void DetermineNextWayPointAdvanced()
    {
        // Advance to next waypoint
        if (tankPatrol == TankPatrol.aiPingPong)
        {
            // Ping pong is the only method that doesn't always move forward
            // Move to the "next" waypoint -- but "next" depends on what direction I'm moving.
            if (isAdvancingWaypoints)
            {
                //Add meaning next way point
                currentWaypoint++;
            }
            else
            {
                //Substract back to a different waypoint
                currentWaypoint--;
            }
        }
        else
        {
            //Add
            currentWaypoint++;
        }

        //Loop back
        if (currentWaypoint >= waypoints.Count)
        {
            //Modify the Tank Range where it doesn't get an error for my loop if not equal to.
            if (tankPatrol != TankPatrol.aiLoop)
            {
                if (tankPatrol == TankPatrol.aiPingPong)
                {
                    // Reverse directions
                    isAdvancingWaypoints = false;
                    currentWaypoint = currentWaypoint - 2;
                }
            }
            else
            {
                //Loop to 0 from 0-3
                currentWaypoint = 0;
            }
        }
        
        else if (currentWaypoint < 0)
        {
            //Back to the waypoint
            isAdvancingWaypoints = true;
            currentWaypoint = 1;
        }
    }
    //Can Move Forward
    bool CanMoveForward()
    {
        //Raycast is a view based on the.
        if (Physics.Raycast(data.motor.tf.position, data.motor.tf.forward, avoidDistance))
        {
            //false if something in front
            return false;
        }
        //true
        return true;
    }
    //Tank Move Forward
    public void MoveTowards(Vector3 target)
    {
        //Normal State
        if (avoidState == AvoidState.Normal)
        {
            if (CanMoveForward())
            {
                //Target Position of the new target
                Vector3 targetPosition = new Vector3(target.x, data.motor.tf.position.y, target.z);
                //Direct to the current waypoint.
                Vector3 dirToWaypoint = targetPosition - data.motor.tf.position;
                //TurnTowards to the current waypoint.
                data.motor.TurnTowards(dirToWaypoint);
                //Move Forward based from the data (speed) and motor (forward formula)
                data.motor.Move(data.motor.tf.forward);
            }
            //Nothing else, turn to avoid.
            else
            {
                avoidState = AvoidState.TurnToAvoid;
            }
        }
        //Turn To Avoid
        else if (avoidState == AvoidState.TurnToAvoid)
        {
            //Turn Motor
            data.motor.Turn(3);
            //Move Forward
            if (CanMoveForward())
            {
                avoidState = AvoidState.MoveToAvoid;
                avoidStateTime = Time.time;
            }
        }
        //Move to Avoid meaning you're moving away from the obstacle
        else if (avoidState == AvoidState.MoveToAvoid)
        {
            //Can Move Forward
            if (CanMoveForward())
            {
                data.motor.Move(data.motor.tf.forward);
            }
            //Turn to Avoid
            else
            {
                avoidState = AvoidState.TurnToAvoid;
            }
            //Time to go back to normal
            if (Time.time >= avoidStateTime + avoidDuration)
            {
                avoidState = AvoidState.Normal;
            }
        }
    }
}
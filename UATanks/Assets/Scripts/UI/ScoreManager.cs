﻿//Norman Nguyen
//Score Manager: Manage score contents
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    //Text
    public Text scoreText;
    public Text highScoreText;
    public TankData data;
    //High Score
    public int highScore;
	// Use this for initialization
	void Start ()
	{
	    scoreText = GetComponent<Text>();
	    data = GetComponent<TankData>();
        data.currentScore = 0;
        //Find Text
        scoreText = GameObject.Find("P1Score").GetComponent<Text>();
        //Find High Score Text
	    highScoreText = GameObject.Find("HighScoreText").GetComponent<Text>();
        highScore = PlayerPrefs.GetInt("highscore", highScore);
        setUIScore();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    setUIScore();
        stopScore();
    }
    //UI Score
    public void setUIScore()
    {
        scoreText.text = "Score: " + data.currentScore.ToString();
        highScoreText.text = "High Score: " + highScore.ToString();
    }
    public void stopScore()
    {
        PlayerPrefs.SetInt("score", highScore);
        if (PlayerPrefs.HasKey("highScore"))
        {
            if (highScore > PlayerPrefs.GetInt("highScore"))
            {
                PlayerPrefs.SetInt("highScore", highScore);
            }
        }
        else
        {
            PlayerPrefs.SetInt("highScore", highScore);
        }
    }
}

﻿//Norman Nguyen
//Tank Controllers: Add movement, and fire.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Required Components from three Tank files.
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(TankCannon))]
public class TankController : MonoBehaviour
{
    private TankMotor motor; //Motor
    public TankData data; //Data
    private TankCannon cannon; //Cannon
    public Color color; //Color
    public MeshRenderer rend; //Mesh Renderer
    public enum tankControls
    {
        wasd,
        arrows
    }

    public tankControls controlTypes;
    // Start the program to get the data from TankMotor.cs and TankData.cs
    void Start()
    {
        //Get Component from Motor, Data and Cannon for the tank controls
        motor = GetComponent<TankMotor>();
        data = GetComponent<TankData>();
        cannon = GetComponent<TankCannon>();
        GameManager.instance.player = this;
        rend.material.color = color;
    }
    void Update()
    //Tank Controls with WASD.
    {

        switch (controlTypes)
        {
            case tankControls.wasd:
                player1();
                break;
            case tankControls.arrows:
                player2();
                break;
        }
        //Forward (+)
    }
    void player1()
    {
        if (Input.GetKey(KeyCode.W))
        {
            motor.Move(data.motor.tf.forward);
        }
        // Backwards (-)
        if (Input.GetKey(KeyCode.S))
        {
            motor.Reverse(data.motor.tf.forward);
        }
        //Left rotation (-)
        if (Input.GetKey(KeyCode.A))
        {
            motor.Turn(-data.turnSpeed);
        }
        //Right rotation (+)
        if (Input.GetKey(KeyCode.D))
        {
            motor.Turn(data.turnSpeed);
        }
        //Fire bullets from the tank
        if (Input.GetKey(KeyCode.LeftControl))
        {
            cannon.Fire();
        }
        //Fire bullets using the left mouse button
        if (Input.GetKey(KeyCode.Mouse0))
        {
            cannon.Fire();
        }
        //Change to Normal
        if (Input.GetKey(KeyCode.Alpha1))
        {
            data.shootRate = 1f;
            color = Color.white;
            rend.material.color = color;
            data.currentBullet = 0;
        }
        //Change to Rapid
        if (Input.GetKey(KeyCode.Alpha2))
        {
            data.shootRate = .1f;
            color = Color.black;
            rend.material.color = color;
            data.currentBullet = 1;
        }
    }
    void player2()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            motor.Move(data.motor.tf.forward);
        }
        // Backwards (-)
        if (Input.GetKey(KeyCode.DownArrow))
        {
            motor.Reverse(data.motor.tf.forward);
        }
        //Left rotation (-)
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            motor.Turn(-data.turnSpeed);
        }
        //Right rotation (+)
        if (Input.GetKey(KeyCode.RightArrow))
        {
            motor.Turn(data.turnSpeed);
        }
        //Fire bullets from the tank
        if (Input.GetKey(KeyCode.RightControl))
        {
            cannon.Fire();
        }
        //Fire bullets using the left mouse button
        //Change to Normal
        if (Input.GetKey(KeyCode.Alpha3))
        {
            data.shootRate = 1f;
            color = Color.white;
            rend.material.color = color;
            data.currentBullet = 0;
        }
        //Change to Rapid
        if (Input.GetKey(KeyCode.Alpha4))
        {
            data.shootRate = .1f;
            color = Color.black;
            rend.material.color = color;
            data.currentBullet = 1;
        }
    }
}

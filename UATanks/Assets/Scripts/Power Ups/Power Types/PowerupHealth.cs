﻿//Norman Nguyen
//Powerup Health
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
//Inherit from the Powerup base class
public class PowerupHealth : Powerup
{
    //Bonus Amount
    public int bonusAmount;
    //Clone
    public PowerupHealth(PowerupHealth powerupToClone)
    {
        bonusAmount = powerupToClone.bonusAmount;
        countdown = powerupToClone.countdown;
        isPerm = powerupToClone.isPerm;

        powerupType = PowerupType.HealthPowerup;
    }

    //Override the function
    public override void OnAddPowerup(TankData data)
    {
        //Add to health
        data.health.currentHealth += bonusAmount;
        //base function to add
        base.OnAddPowerup(data);
    }
    public override void OnRemovePowerup(TankData data)
    {
        //Set Permanent
        if (isPerm)
            return;
        //base function to remove
        base.OnRemovePowerup(data);
        //Remove Health
        data.health.currentHealth -= bonusAmount;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoresUI : MonoBehaviour
{
    public RectTransform highScore;
    public Text hsText;
    public Vector3 singlePlayer = new Vector3(-767, 390, 0);
    public Vector3 multiPlayer = new Vector3(0, 0, 0);
	// Use this for initialization
	void Start () {
        highScore = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿//Norman Nguyen
//Bullet Data: Contains the timer (how long the bullet last), damage, and speed data (for the shoot rate in tank data).
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{//Audio Source
    public AudioSource bulletCollision;
    //Audio Clip
    public AudioClip clip;
    //Add thrust
    public float thrust;
    //timer
    public float timer = 3.0F;
    //bullet damage
    public int bulletDamage = 10;
    //speed for the bullet to the tank data shoot rate.
    [HideInInspector]public float speed;
    //currentScore game object where the score earns from bullet not the tank
    //One bullet equals to 10 points for the player tank
    public GameObject currentScore;
    private void Start()
    {
        bulletCollision = GetComponent<AudioSource>();
    }
    //Wakes up the destroy method if the bullet fires
    void Awake ()
    {
        Destroy(gameObject, timer);
    }
    //Collision happens if the bullet collides an object.
    //Milestone 4: Now with Audio
    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
        AudioSource.PlayClipAtPoint(clip, transform.position, GameManager.instance.sfxVolume);
    }
}
﻿//Norman Nguyen
//Tank Health: Data of tanks health
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{

    public AudioClip clip;
    private TankData data;
    public GameObject deathExplosion;
    public Text p1Health;

    //Variables for health and damage
    public int currentHealth = 100;
    public float maxHealth = 100;
    //Update on the health system.
    void Update()
    {
        //Destroy Tank if it hits to zero
        if (gameObject != null)
        {
            if (currentHealth == 0)
            {
                AudioSource.PlayClipAtPoint(clip, transform.position, GameManager.instance.sfxVolume);
                Explode();
                Destroy(this.gameObject);
            }
        }
    }
    //Explosion sound
    void Explode()
    {
        GameObject explosion = Instantiate(deathExplosion, transform.position, Quaternion.identity);
        explosion.GetComponent<ParticleSystem>().Play();
    }
}

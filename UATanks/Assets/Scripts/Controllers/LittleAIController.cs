﻿//Norman Nguyen
//This is the Little Guy AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AIController))]
public class LittleAIController : MonoBehaviour
{
    public TankData data; //Tank Data
    public AIController controller; //AI Controller
    //States
    public enum AIStates
    {
        Idle, Flee
    }
    //Change States
    public AIStates currentState;
    public float timeInCurrentState; //Time Current State
    public float fleeDistance = 5; //Flee Distance

    public float fleeTime = 10; //Flee Time
    Transform aiTransform;
    Transform playerTransform;
    Transform playerTransform2;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
    }
    // Update is called once per frame
    void Update()
    {
        //Current Time State match delta time.
        timeInCurrentState += Time.deltaTime;
        //Player Transform by the game manager
        playerTransform = GameManager.instance.player.data.motor.transform;
        playerTransform2 = GameManager.instance.player2.data.motor.transform;
        //AI Transform
        aiTransform = data.motor.transform;
        switch (currentState)
        {
            //Idle State
            case AIStates.Idle:
                Idle();
                if (Vector3.Distance(aiTransform.position, playerTransform.position) < fleeDistance)
                {
                    ChangeState(AIStates.Flee);
                }
                if (playerTransform2 != null)
                {
                    if (Vector3.Distance(aiTransform.position, playerTransform2.position) < fleeDistance)
                    {
                        ChangeState(AIStates.Flee);
                    }
                }
                
                break;
            //Flee State
            case AIStates.Flee:
                Flee();
                Flee2();
                if (timeInCurrentState > fleeTime)
                {
                    ChangeState(AIStates.Idle);
                }
                break;
        }
    }
    //Change States
    public void ChangeState(AIStates newState)
    {
        //Set the state
        currentState = newState;
        //Reset the timer back to zero.
        timeInCurrentState = 0;
    }
    //Leave it be
    void Idle()
    {

    }
    //Flee Method for the Little Tank
    void Flee()
    {
        //Vector 3 from vector to player positions
        Vector3 vectorToPlayer = playerTransform.position - aiTransform.position;
        //Away from the player
        vectorToPlayer = -1 * vectorToPlayer;
        //Make the Vector3 Normalize to 1
        vectorToPlayer.Normalize();
        //To move towards the new point
        controller.MoveTowards(vectorToPlayer);
    }
    void Flee2()
    {
        //Vector 3 from vector to player positions
        Vector3 vectorToPlayer2 = playerTransform2.position - aiTransform.position;
        //Away from the player
        vectorToPlayer2 = -1 * vectorToPlayer2;
        //Make the Vector3 Normalize to 1
        vectorToPlayer2.Normalize();
        //To move towards the new point
        controller.MoveTowards(vectorToPlayer2);
    }
}

﻿//Norman Nguyen
//Pick Up Rapid Fire
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupRapid : MonoBehaviour {
    //Rapid Fire Powerup
    public PowerupRapidFire powerup;

    public AudioClip clip;
    //On Trigger event where the tank collides (REMEMBER TO TURN ON TRIGGER)
    private void OnTriggerEnter(Collider other)
    {
        //Link from the Powerup Manager for Get Component
        PowerupManager pm = other.GetComponent<PowerupManager>();
        //If null
        if (pm != null)
        {
            //Add Powerup
            pm.AddPowerup(powerup);
            // Destroy this powerup
            Destroy(gameObject);
        }
        if (clip != null)
        {
            AudioSource.PlayClipAtPoint(clip, transform.position, GameManager.instance.sfxVolume);
        }
    }
}

﻿//Norman Nguyen
//Pick Up Speed
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpeed : MonoBehaviour {

    //Speed Powerup
    public PowerupSpeed powerup;

    public AudioClip clip;

    //On Trigger event where the tank collides (REMEMBER TO TURN ON TRIGGER)
    void OnTriggerEnter(Collider other)
    {
        //Link from the Powerup Manager for Get Component
        PowerupManager pm = other.GetComponent<PowerupManager>();
        //If null
        if (pm != null)
        {
            //Add Powerup
            pm.AddPowerup(powerup);
            // Destroy this powerup
            Destroy(gameObject);
        }
        if (clip != null)
        {
            AudioSource.PlayClipAtPoint(clip, transform.position, GameManager.instance.sfxVolume);
        }
    }
}

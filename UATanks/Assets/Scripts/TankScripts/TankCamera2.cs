﻿//Norman Ngugyen
//Tank Camera2: A camera that follows the tank as it moves and rotates.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCamera2 : MonoBehaviour
{
    public Transform player;
    //Offset the vector to allow the camera to not tie to the tank
    public Vector3 offset;
    //Turn Speed for the camera
    public float turnSpeed;
    public GameObject playerPrefab;
    //Moves the tank while updating on the position for the camera.
    void Update()
    {
        player = GameObject.Find("Tank2").transform;
        //Camera moves as it follows the cannon
        //Updating on the position of the character and camera
        transform.position = player.position + offset;
        //Positioning to look at the player
        transform.LookAt(player.transform);
        transform.position = player.transform.position + offset;
        //Left Arrow Key
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.RotateAround(player.transform.position, Vector3.up, -50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
        //Right Arrow Key
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.RotateAround(player.transform.position, Vector3.up, 50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
        //N Key camera (THIS IS WHEN IF THE CAMERA IS NOT ORIENTATED)
        if (Input.GetKey(KeyCode.N))
        {
            transform.RotateAround(player.transform.position, Vector3.up, -50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
        //M Key camera (THIS IS WHEN IF THE CAMERA IS NOT ORIENTATED)
        if (Input.GetKey(KeyCode.M))
        {
            transform.RotateAround(player.transform.position, Vector3.up, 50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
    }
}
﻿//Norman Nguyen
//Powerup Speed
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
//Inherit from the Powerup base class
public class PowerupSpeed : Powerup
{
    //Bonus Amount
    public float bonusAmount;
    //Clone
    public PowerupSpeed(PowerupSpeed powerupToClone)
    {
        //Clone Amout
        bonusAmount = powerupToClone.bonusAmount;
        countdown = powerupToClone.countdown;
        isPerm = powerupToClone.isPerm;

        powerupType = PowerupType.SpeedPowerup;
    }
    //Override the function
    public override void OnAddPowerup(TankData data)
    {
        //Add Speed
        data.moveSpeed += bonusAmount;
        //base function to add
        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup(TankData data)
    {
        //base function to remove
        base.OnRemovePowerup(data);
        // Remove from Speed
        data.moveSpeed -= bonusAmount;
    }
}

﻿//Norman Nguyen
//Bullet Controller: Adds the bullet force and rigidbody.
//Added sound and VFX
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletController : MonoBehaviour
{
    public TankData data;
    //Transform
    private Transform tf;
    //Rigibody for the bullet
    private Rigidbody rb;
    //Bullet Data
    private BulletData bd;
    //Particle System
    public GameObject collisonExplosion;
    // Start the script with all the components and AddForce for the bullet.
    void Start()
    {
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        bd = GetComponent<BulletData>();
        data = GetComponent<TankData>();
        //Add force for the rigidbody for the bullet
        AddForce();
    }
    void Update()
    {
        Move();
    }
    //Move helps to add the bullet speed to shoot forward.
    void Move()
    {
        tf.position += tf.forward * bd.speed * Time.deltaTime;
    }
    //Add force to allow the bullet from the rigidbody
    public void AddForce()
    {
        rb.AddForce(transform.forward * bd.thrust);
    }
    //Collision event
    public void OnCollisionEnter(Collision collision)
    {
        //Tag enemies so it doesn't harm the walls like an enemy
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Player"))
        {
            //If the bullet collided the tank
            collision.gameObject.GetComponent<TankHealth>().currentHealth -= bd.bulletDamage;
            //Adds score if the bullet damages the tank.
            bd.currentScore.GetComponent<TankData>().currentScore += 10;
            //Summon Explosion
            Explode();
            //If the enemy tank is destroyed, you will be rewarded 100 points.
            if (collision.gameObject.GetComponent<TankHealth>().currentHealth <= 0)
            {
                bd.currentScore.GetComponent<TankData>().currentScore += collision.gameObject.GetComponent<TankData>().pointsWorth;
            }
        }
    }
    //Added Explosion
    void Explode()
    {
        GameObject firework = Instantiate(collisonExplosion, transform.position, Quaternion.identity);
        firework.GetComponent<ParticleSystem>().Play();
    }
}

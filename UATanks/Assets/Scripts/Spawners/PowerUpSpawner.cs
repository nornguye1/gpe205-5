﻿//Norman Nguyen
//Spawn Powerups
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
	//GameObject for a specific powerup
	public GameObject objectToSpawn;
	//GameObject spawned in game/map
	public GameObject spawnedObject;
	//Transform
	private Transform tf;
    //Time Between
	public float timeBetweenSpawns;

	private float spawnCountdown;
	// Array of locations to spawn to
	public Transform[] spawnLocations;
	// Use this for initialization
	void Start()
	{
		tf = GetComponent<Transform>();

		spawnCountdown = timeBetweenSpawns;
	}
	// Update is called once per frame
	void Update()
	{
		//If there is there nothing currently spawned
		if (spawnedObject == null)
		{
			spawnCountdown -= Time.deltaTime;
			//Countdown
			if (spawnCountdown <= 0)
			{
                //Using Spawning locations within the range
				int locationId = Random.Range(0, spawnLocations.Length);
				Transform spawnTransform = spawnLocations[locationId];
				//Spawn powerup object
				spawnedObject = Instantiate(objectToSpawn, spawnTransform.position, spawnTransform.rotation) as GameObject;
                //Reset timer for the next powerup after being obtained
				spawnCountdown = timeBetweenSpawns;
			}
		}
	}
}